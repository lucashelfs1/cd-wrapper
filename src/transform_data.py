# Transform data from CD since 2018
from transformer import YearTransformer


def transform_year_range(start_year, end_year):
    """
    Transform collected data for the specified year range.
    """
    for year in range(start_year, end_year):
        Transformer = YearTransformer(year)
        Transformer.collapse_data()


if __name__ == '__main__':
    start_year = 2018
    end_year = 2022
    transform_year_range(start_year, end_year)
