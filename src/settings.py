# Filepaths and settings

index_csv = './raw_data/indexes/votings_{year}_{month}.csv'

infos_json = './raw_data/infos/{id}.json'

votes_json = './raw_data/votes/{id}.json'

transformed_year_votes_filename = './transformed_data/votes_{year}.csv'

transformed_year_infos_filename = './transformed_data/votings_{year}.csv'

transformed_year_propositions_filename = './transformed_data/propositions_{year}.csv'
