import pandas as pd
from settings import transformed_year_votes_filename, transformed_year_propositions_filename, transformed_year_infos_filename, transformed_year_propositions_filename


def gather_entities_from_votes(year):
    """
    Gather entities from the transformed votes for an year file.
    """
    df = pd.read_csv(transformed_year_votes_filename.format(year=year))
    votes_cols = ['tipoVoto', 'dataRegistroVoto', 'deputado_id', 'idVotacao']
    deputies_cols = ['deputado_id', 'deputado_uri', 'deputado_nome', 'deputado_siglaPartido', 'deputado_siglaUf', 'deputado_idLegislatura', 'deputado_urlFoto', 'deputado_email']
    parties_cols = ['deputado_siglaPartido', 'deputado_uriPartido']
    votes = df[votes_cols]
    deputies = df[deputies_cols]
    parties = df[parties_cols]
    return votes, deputies, parties


def gather_entities_from_propositions(year):
    """
    Gather entities from the transformed propositions for an year file.
    """
    df = pd.read_csv(transformed_year_propositions_filename.format(year=year))
    return df


def gather_entities_from_infos(year):
    """
    Gather entities from the transformed infos for an year file.
    """
    df = pd.read_csv(transformed_year_infos_filename.format(year=year))
    return df


def fetch_all_entities_year_range(start_year, end_year):
    """
    Fetch all desired entities for an year.
    """
    votes_list = []
    deputies_list = []
    parties_list = []
    propositions_list = []
    votings_list = []

    for year in range(start_year, end_year):
        votes, deputies, parties = gather_entities_from_votes(year)
        propositions = gather_entities_from_propositions(year)
        votings = gather_entities_from_infos(year)
        votes_list.append(votes)
        deputies_list.append(deputies)
        parties_list.append(parties)
        propositions_list.append(propositions)
        votings_list.append(votings)

    df_votes = pd.concat(votes_list)
    df_deputies = pd.concat(deputies_list).drop_duplicates()
    df_parties = pd.concat(parties_list).drop_duplicates()
    df_propositions = pd.concat(propositions_list).drop_duplicates()
    df_votings = pd.concat(votings_list).drop_duplicates()

    df_votes.to_csv("transformed_data/entitiy_votos.csv", index=False)
    df_deputies.to_csv("transformed_data/entitiy_deputados.csv", index=False)
    df_parties.to_csv("transformed_data/entitiy_partidos.csv", index=False)
    df_propositions.to_csv("transformed_data/entitiy_proposicoes.csv", index=False)
    df_votings.to_csv("transformed_data/entitiy_votacoes.csv", index=False)



if __name__ == '__main__':
    start_year = 2018
    end_year = 2022
    fetch_all_entities_year_range(start_year, end_year)
