import datetime
import logging

from abc import ABC, abstractmethod
from helpers import *
from settings import transformed_year_votes_filename, transformed_year_infos_filename, transformed_year_propositions_filename, infos_json, votes_json


logging.basicConfig(filename='logs/transformer.log', filemode='a', level=logging.INFO)


class IdTransformer(ABC):
    """
    Abstract class for the CD data transformation of id named files.
    """

    ids = []

    def __init__(self, ids):
        self.ids = ids

    @abstractmethod
    def collapse_data(self):
        """
        Collapse all data for the ids into a pandas DataFrame, given the desired endpoint information.
        """
        pass


class VotesTransformer(IdTransformer):
    """
    Transform all votes for the given ids.
    :param ids: The list of file ids to be transformed.
    """

    def collapse_data(self):
        """
        Load and collapse all votes.
        """
        infos = []
        for id in self.ids:
            filename = votes_json.format(id=id)
            votes = load_json(filename)
            if len(votes) != 0:
                for vote in votes:
                    vote['idVotacao'] = id
                    infos.append(flatten(vote))
        return pd.DataFrame(infos)


class InfosTransformer(IdTransformer):
    """
    Transform all information for the given ids.
    :param ids: The list of file ids to be transformed.
    """

    def match_description(self, info_dict):
        """
        Match the most relevant description available on the informations of a voting.
        """
        if 'descricao' in info_dict['ultimaApresentacaoProposicao'] and str(info_dict['ultimaApresentacaoProposicao']['descricao']) != 'None':
            return info_dict['ultimaApresentacaoProposicao']['descricao']
        elif len(info_dict['proposicoesAfetadas']) > 0:
            return info_dict['proposicoesAfetadas'][0]['ementa']
        elif len(info_dict['objetosPossiveis']) > 0:
            return info_dict['objetosPossiveis'][0]['ementa']
        else:
            return 'Nenhuma ementa relacionada.'

    def fetch_propositions(self, info_dict):
        """
        Fetch propositions impacted by a voting.
        """
        ids = [proposition['id'] for proposition in info_dict['objetosPossiveis']]
        return ids

    def collapse_data(self):
        """
        Load and collapse all voting information.
        """
        infos = []
        for id in self.ids:
            data = {}
            filename = infos_json.format(id=id)
            info = load_json(filename)
            if len(info.keys()) > 0:
                data['idVotacao'] = id
                data['aprovacao'] = info['aprovacao']
                data['data'] = info['data']
                data['descricao'] = self.match_description(info)
                data['idsProposicoes'] = self.fetch_propositions(info)
                infos.append(data)
        return pd.DataFrame(infos)


class PropositionsTransformer(IdTransformer):
    """
    Transform all proposition from the given voting ids.
    :param ids: The list of file ids to be transformed.
    """

    def collapse_data(self):
        """
        Load and collapse all propositions from voting information.
        """
        infos = []
        for id in self.ids:
            filename = infos_json.format(id=id)
            info = load_json(filename)
            if 'objetosPossiveis' in info:
                for proposition in info['objetosPossiveis']:
                    infos.append(proposition)

        return pd.DataFrame(infos)


class YearTransformer():

    """
    Transform all votes and informations of given year, stored in pages.
    :param year: The year of the data to be transformed.
    """

    year = datetime.datetime.today().year

    def __init__(self, year):
        self.year = year

    def collapse_year_index(self):
        """
        Collapse an year index given all votings by month.
        """
        dfs = []
        for month in range(1, 13):
            index_filename = index_csv.format(year=self.year, month=month)
            try:
                dfs.append(pd.read_csv(index_filename))
            except BaseException:
                pass
        return pd.concat(dfs)

    def collapse_data(self):
        """
        Collapse an votings and informations for an year.
        """

        logging.info(f"Collapsing {self.year} data.")
        df = self.collapse_year_index()
        ids = df.id.tolist()

        votes_filename = transformed_year_votes_filename.format(year=self.year)
        infos_filename = transformed_year_infos_filename.format(year=self.year)
        props_filename = transformed_year_propositions_filename.format(year=self.year)

        logging.info(f"Collapsing {self.year} votes.")
        VoteTrans = VotesTransformer(ids)
        votes = VoteTrans.collapse_data()
        votes.to_csv(votes_filename, index=False)

        logging.info(f"Collapsing {self.year} informations.")
        InfoTrans = InfosTransformer(ids)
        infos = InfoTrans.collapse_data()
        infos.to_csv(infos_filename, index=False)

        logging.info(f"Collapsing {self.year} needed propositions.")
        PropTrans = PropositionsTransformer(ids)
        infos = PropTrans.collapse_data()
        infos.to_csv(props_filename, index=False)
