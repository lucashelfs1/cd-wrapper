import collections
import json
import pandas as pd

from settings import index_csv, infos_json, votes_json



def load_json(filename):
    """
    Load data from json file.
    """
    try:
        f = open(filename, "r")
        data = json.loads(f.read())
        f.close()
        return data['dados']
    except BaseException:
        return {}


def flatten(dictionary, parent_key=False, separator=''):
    """
    Turn a nested dictionary into a flattened dictionary
    :param dictionary: The dictionary to flatten
    :param parent_key: The string to prepend to dictionary's keys
    :param separator: The string used to separate flattened keys
    :return: A flattened dictionary
    """
    items = []
    for key, value in dictionary.items():
        new_key = str(parent_key) + separator + key if parent_key else key
        if isinstance(value, collections.MutableMapping):
            items.extend(flatten(value, new_key, separator).items())
        elif isinstance(value, list):
            for k, v in enumerate(value):
                items.extend(flatten({str(k): v}, new_key).items())
        else:
            items.append((new_key, value))
    return dict(items)
