import calendar
import requests
import pandas as pd
import logging
import time 
import json

from typing import List, Tuple


logging.basicConfig(filename='logs/fetching_data.log', filemode='a', level=logging.INFO)


def number_of_days_in_month(year: int, month: int):
    ''' Get number of days in a given month. '''
    return calendar.monthrange(year=year, month=month)[1]


def save_json(filename: str, data: dict):
    ''' Save data from dict in a file with specified filename. '''
    with open(filename, 'w') as f:
            json.dump(data, f)


class DataFetcher:
    data = []
    base_url = "https://dadosabertos.camara.leg.br/api/v2/"
    headers = {'Content-type': 'application/json'}
    def __init__(self):
        pass
    def create_df(self, filename: str) -> None:
        ''' Create dataframe with votings data. '''
        df = pd.DataFrame(self.data)
        df.to_csv(filename, index=False)
    def load_df(filename: str) -> pd.DataFrame:
        ''' Load dataframe with votings data. '''
        return pd.read_csv(filename)
    def check_if_pagination_ended(self, links: List[dict]) -> Tuple:
        ''' Check if search has link for next page. '''
        ended = False
        for link in links:
            if 'next' in link['rel']:
                next_link = link['href']
                return ended, next_link
        ended = True
        return ended, ''


class VotingFetcher(DataFetcher):
    def fetch_voting_index_data(self, year: int, month: int) -> None:
        ''' Fetch index of votings of brazilian chamber of deputies for a given month and year. '''
        logging.info(f'Fetching all votings for brazilian chamber of deputies for month {month} in {year}.')
        ended = False
        data = []
        last_day = number_of_days_in_month(year, month)
        month = str(month).zfill(2)
        last_day = str(last_day).zfill(2)
        votings_url = f'votacoes?dataInicio={year}-{month}-01&dataFim={year}-{month}-{last_day}&ordem=DESC&ordenarPor=dataHoraRegistro'
        url = self.base_url + votings_url
        while not ended:
            try:
                response = requests.get(url, headers=self.headers)
                assert response.status_code == 200
                response = response.json()
                links = response['links']
                data.append(response['dados'])
                ended, url = self.check_if_pagination_ended(links)
            except AssertionError:
                logging.info(f'Assertion error, backing off for a few seconds.')        
                time.sleep(10)
        self.data = [item for sublist in data for item in sublist]
        number_of_votings = len(self.data)
        logging.info(f'Fetched {number_of_votings} votings for brazilian chamber on month {month} in {year}.')
        return True

    def load_ids_from_voting_index(self, year: int, month: int) -> List:
        ''' Load voting ids from index of votings for a given month and year. '''
        filename = f'raw_data/indexes/votings_{year}_{month}.csv'
        try:
            df = pd.read_csv(filename)
            ids = df.id.tolist()
            return ids
        except:
            logging.info(f"No data on file: {filename}")
            return []

    def fetch_voting_index(self, year: int, month: int) -> None:
        ''' Save individual index file for all votings in a given month and year. '''
        fetched_data = self.fetch_voting_index_data(year, month)
        if fetched_data:
            filename = f'raw_data/indexes/votings_{year}_{month}.csv'
            self.create_df(filename)

    def fetch_voting_votes(self, year: int, month: int) -> None:
        ''' Save individual files of votes for votings in a given month and year. '''
        ids = self.load_ids_from_voting_index(year, month)
        for id in ids:
            try:
                logging.info(f'Downloading votes: {id}')
                url_votes_by_id = f'votacoes/{id}/votos'
                response = requests.get(self.base_url + url_votes_by_id, headers=self.headers)
                assert response.status_code == 200
                data = response.json()
                fname = f"raw_data/votes/{id}.json"
                save_json(fname, data)
            except AssertionError:
                logging.info(f'Assertion error, backing off for a few seconds.')        
                time.sleep(10)

    def fetch_voting_information(self, year: int, month: int) -> None:
        ''' Save individual files of voting informations for a given month and year. '''
        ids = self.load_ids_from_voting_index(year, month)
        for id in ids:
            try:
                logging.info(f'Downloading voting: {id}')
                url_voting_by_id = f'votacoes/{id}'
                response = requests.get(self.base_url + url_voting_by_id, headers=self.headers)
                assert response.status_code == 200
                data = response.json()
                fname = f"raw_data/infos/{id}.json"
                save_json(fname, data)
            except AssertionError:
                logging.info(f'Assertion error, backing off for a few seconds.')        
                time.sleep(10)

    def fetch_voting_orientations(self, year: int, month: int) -> None:
        ''' Save individual files of party voting orientations for a given month and year. '''
        ids = self.load_ids_from_voting_index(year, month)
        for id in ids:
            try:
                logging.info(f'Downloading orientations: {id}')
                url_orientations_by_id = f'votacoes/{id}/orientacoes'
                response = requests.get(self.base_url + url_orientations_by_id, headers=self.headers)
                assert response.status_code == 200
                data = response.json()
                fname = f"raw_data/orientations/{id}.json"
                save_json(fname, data)
            except AssertionError:
                logging.info(f'Assertion error, backing off for a few seconds.')        
                time.sleep(10)

