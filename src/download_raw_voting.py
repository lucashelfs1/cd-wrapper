# Download all voting data since 2018
from fetcher import VotingFetcher


def download_year_range(start_year, end_year):
    fetcher = VotingFetcher()
    # Indexes
    for year in range(start_year, end_year):
        for month in range(1, 13):
            fetcher.fetch_voting_index(year, month)
    # Informations
    for year in range(start_year, end_year):
        for month in range(1, 13):
            fetcher.fetch_voting_information(year, month)
    # Orientations
    for year in range(start_year, end_year):
        for month in range(1, 13):
            fetcher.fetch_voting_orientations(year, month)
    # Votes
    for year in range(start_year, end_year):
        for month in range(1, 13):
            fetcher.fetch_voting_votes(year, month)


if __name__ == '__main__':
    start_year = 2018
    end_year = 2022

