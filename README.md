# cd-wrapper

Code for getting information about votings in the brazilian Chamber of Deputies (API v2).

## Installation
To install the required libraries for this project, run the following command:

```bash
$ pip install -r requirements.txt
```

If you don't want to mess with your current default Python environment, run on a virtual environment.

## Usage
For running this project, you must follow the right order:

```bash
$ cd scripts
$ ./download_data.sh
$ ./transform_data.sh
```

The output files will be located in the *src/transformed_data/* folder, with the prefix *entity*.
## Support
For any doubts related to this project you can send me an email: lucashelfs@ime.usp.br

## Data Structure
The Entities data structure can be seen in this UML Class Diagram:

![UML](/docs/uml.jpg "Entidades criadas")